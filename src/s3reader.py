import boto
import json
import csv

import datetime

def get_event_bucket():
    """Creates a new connection and retuns the bitrec.optprice.events bucket"""
    con = boto.connect_s3(
        aws_access_key_id='AKIAIYEPYRWS7KJYJAHQ',
        aws_secret_access_key='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')
    
    return con.get_bucket('bitrec.whatdowecallthis.events')

def log_stream(ls):
    """Given an iterable of s3 keys, yields the parsed json objects."""
    s = ""
    for key in ls:
        if key.last_modified < "2016-04-11":
            continue
        
        for text in key:
            text = (s+text).replace('}{', '}\n{')
            text = text.split('\n')
            for ln in text[:-1]:
                yield json.loads(ln)
            s = text[-1]
    if len(s):
        yield json.loads(s)


"""Throttle class for printing things every X seconds"""

class Throttle(object):
   def __init__(self, delay):
      self.delay = delay
      self.last = None
   
   def __call__(self):
      if self.last is None or datetime.datetime.now() - self.last > self.delay:
          self.last = datetime.datetime.now()
          return True
      else:
          return False

def get_format(event, data):
    if data.has_key("fmt"):
        return data["fmt"].split(",") #future b-scripts should log a fmt parameter
    else:
        fmt = []
        
        if data.get("event") == "error":
            fmt.append("error")
        
        if data.get("event") == "add-to-cart": #TODO: for shops where we have checkout parsing, return add-to-cart
            fmt.append("visitor-purchased-item")
        
        if data.get("event") == None and data.has_key("item"): 
            if data["shop"] == "4" and data["price"] == 0 and data["item"][0] == "/":
                fmt.append("click-sold-out-manoknyga") #recognize manoknyga sold out items
            else:
                fmt.append("click-item")
        
        if data.has_key("visitor") and data.has_key("code"):
            fmt.append("use-code")
        
        return fmt
        #TODO: recognize all the different start-checkout and end-checkout logs

def for_each_event(logs, parsers):#TODO: accept a parser array, use that to  
    """Given an iterable of events, calls func for each inv event that has a visitor,
    prints any exceptions."""
    
    throttle = Throttle(datetime.timedelta(seconds=5))
    
    for event in logs:
        try:
            if 'args' not in event or 'data' not in event['args']:
                continue
            
            data = event['args']['data']
            data['fmt'] = get_format(event, data)
            
            if throttle():
                print event["@timestamp"]
            
            for p in parsers:
                try:
                    p.read(event, data)
                
                except KeyboardInterrupt:
                    return
                
                except Exception, e:
                    print "ERROR", e, "while parsing", event, "with", p, "(" + str(p.shop_id) + ")"

        except KeyboardInterrupt:
            return
        
        except Exception, e:
            print "ERROR", e, "while reading", event

def write_csv(name, columns, rows):
    """Writes a csv table bith given columns and rows"""
    
    def convert(r):
        if isinstance(r, unicode):
            return r.encode("utf8")
        return r
    
    with open(name, "w") as f:
        writer = csv.writer(f)
        writer.writerow(columns)
        
        for row in rows:
            writer.writerow([convert(r) for r in row])

def to_float(val):
    return float(val.replace(",", "."))

class EventParser(object):
    """Passed to for_each_event and receives all relevant events"""
    
    def read(self, event, data):
        pass

class LogDumper(EventParser):
    def __init__(self, shop_id, file_prefix):
        self.shop_id = shop_id
        self.file_prefix = file_prefix
        self.events = []
    
    def read(self, event, data):
        if data.get("shop") != str(self.shop_id):
            return
        
        self.events.append(event)
    
    def write(self):
        with open(self.file_prefix + "_log.dump", "w") as f:
            for e in sorted(self.events, key=lambda e: e["@timestamp"]):
                f.write(json.dumps(e) + "\n")

class TableParser(EventParser):
    def __init__(self, shop_id, file_prefix, is_shopify=False):
        self.shop_id = shop_id
        self.file_prefix = file_prefix
        self.is_shopify = is_shopify
        
        self.items = {} #item -> (price (eur), discount (eur), collection, page-url)
        self.purchases = [] #(timestamp, user, cart, item, price, quantity, code) #TODO, add cart number, now that we log some cart pages
        self.last_cart = 0
        self.clicks = [] #(timestamp, user, item, user_agent, referrer), with shopify, also (,cart)
        self.errors = [] #(timestamp, message, stack, page-url)
    
    def read(self, event, data):
        if data.get("shop") != str(self.shop_id):
            return
        
        if "error" in data["fmt"]: #message, stack
            self.errors.append((event["@timestamp"], data["message"], data["stack"], data["referrer"]))
        
        if "click-item" in data["fmt"]: #visitor, item, name, price, prevPrice, collection, collectionName, currency
            self.clicks.append((event["@timestamp"], data["visitor"], data["item"], data["user-agent"], data.get("prevReferrer")))
            
            cu = data["currency"]
            k = {'USD': 0.88, None: 0.88}.get(cu, 1)
            price = max(to_float(data["price"]), 0)
            discount = max(to_float(data["prevPrice"]) - price if data["prevPrice"] and price > 0 else 0, 0)
            self.items[data['item']] = (price, discount, data["collection"], data["referrer"])
            #TODO when setting item, don't overwrite exisisting collection with empty string (in assassinshoodies collection only visible sometimes)
        
        if "click-item-shopify" in data["fmt"]:
            self.clicks.append((event["@timestamp"], data["visitor"], data["item"], data["user-agent"], data.get("prevReferrer"), data.get("cart", "")))
        
        if "add-to-cart-shopify" in data["fmt"]:
            self.clicks.append((event["@timestamp"], data["visitor"], "", "", "", data.get("cart", "")))

        if "click-sold-out-manoknyga" in data["fmt"]: #visitor, item(href)
            #here "item" is window.location.path.
            item = ([i for i, (p, d, c, url) in self.items.items() if data["item"] in (url or "")] or [None])[0]
            if item:
                #we can still log a click. we could even mark the click as sold out, though we wont.
                self.clicks.append((event["@timestamp"], data["visitor"], item, data["user-agent"], data.get("prevReferrer")))
            
        if "visitor-purchased-item" in data["fmt"]:  #visitor, item
            self.last_cart += 1
            price = self.items.get(data["item"], [0])[0]
            self.purchases.append((event["@timestamp"], data["visitor"], self.last_cart, data["item"], price, 1, ""))
        
        if "checkout-full-cart" in data["fmt"]: #visitor, itemN, quantityN
            self.last_cart += 1
            indices = [i[4:] for i in data.keys() if len(i) > 4 and i[:4] == "item"]
            
            for ix in indices:
                item, price = ([(i, p) for i, (p, d, c, url) in self.items.items() if data["item"+ix] in (url or "")] or [(None, 0)])[0]
                if item:
                    self.purchases.append((event["@timestamp"], data["visitor"], self.last_cart, item, price, int(data["quantity"+ix]), ""))
            
            #read itemN, quantityN, where itemN is href, for N in 1...
            # write to purchases, all one cart
        
        if "use-code" in data["fmt"]: #visitor, code
            #TODO: when we have that a code was entered, find the last cart of that user, or all carts in last 24 hours, and update their "code" column
            self.purchases.append((event["@timestamp"], data["visitor"], self.last_cart, "", 0, 0, data["code"])) #XXX this is temporary
            pass
        
        if "finish-checkout" in data["fmt"]: #visitor
            #TODO: find the last cart of that user, or all carts in last 24 hours, and add a "finished" column. this is a big problems when logs aren't sorted
            pass
    
    def write(self):#TODO: do the sorting before parse, not after
        if self.is_shopify:
            write_csv(self.file_prefix + "_clicks.csv", ["timestamp", "visitor", "item", "user-agent", "referrer", "cart"], sorted(self.clicks, key=lambda r: r[0]))
        
        else:
            write_csv(self.file_prefix + "_clicks.csv", ["timestamp", "visitor", "item", "user-agent", "referrer"], sorted(self.clicks, key=lambda r: r[0]))
            write_csv(self.file_prefix + "_items.csv", ["item", "price", "discount", "category", "url"], ((i, p, d, c, u) for i, (p, d, c, u) in self.items.items()))
            write_csv(self.file_prefix + "_purchases.csv", ["timestamp", "visitor", "cart", "item", "price", "quantity", "code"], sorted(self.purchases, key=lambda r: r[0]))
        
        write_csv(self.file_prefix + "_errors.csv", ["timestamp", "message", "stack", "url"], sorted(self.errors, key=lambda r: r[0]))   


class PriceWatcher(EventParser):
    def __init__(self, shop_id, file_prefix):
        self.shop_id = shop_id
        self.file_prefix = file_prefix
        #timestamp, product, variant, price, compare_at_price
        self.events = []
    
    def read(self, event, data):
        if data.get("shop") != str(self.shop_id):
            return
        
        if data.get("event") in ("product-read-script", "product-update-webhook"):
            for vid, va in data.get("variants", []).items():
                self.events.append((event["@timestmamp"], data["product"], vid, va["price"], va["compare_at_price"]))
    
    def write(self):
        all_events = sorted(self.events, key=lambda r: r[0])
        self.events = []
        last_price = {}
        for t, i, v, p, c in all_events:
            key = i + "/" + v
            if key not in last_price or last_price[key] != (p, c):
                self.events.append((t, i, v, p, c))
                last_price[key] = (p, c)
        
        write_csv(self.file_prefix + "_pricing.csv", ["timestamp", "item", "variant", "price", "compare_at_price"], self.events)

