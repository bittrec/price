#!/bin/bash

POST_TO_SLACK=/home/ubuntu/post_to_slack.sh

cd /home/ubuntu/bitrec/optprice/src
. /home/ubuntu/.venvs/optprice/bin/activate

export PRODUCTION_OPTSERVER_SETTINGS=/home/ubuntu/bitrec/optprice/optserver1.conf

START=`date +%s.%N`

python read_s3_logs.py
RET=$?

END=`date +%s.%N`
DIFF=`echo "$END - $START" | bc`

if [ $RET -eq 0 ]
then
    $POST_TO_SLACK "Campaign items updated and csv data dumped ($DIFF s)"
else
    $POST_TO_SLACK "Failed to update campaign items and dump csv data ($DIFF s)"
fi

START=`date +%s.%N`

python read_shopify_api.py
RET=$?

END=`date +%s.%N`
DIFF=`echo "$END - $START" | bc`

if [ $RET -eq 0 ]
then
    $POST_TO_SLACK "Campaign items updated and csv data dumped (shopify) ($DIFF s)"
else
    $POST_TO_SLACK "Failed to update campaign items and dump csv data (shopify) ($DIFF s)"
fi
