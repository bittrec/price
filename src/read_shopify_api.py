from optserver.models import Shop
from optserver import *
from optserver.core import logstash_logger
import csv
import json
from datetime import datetime, timedelta

def write_csv(name, columns, rows):
    """Writes a csv table bith given columns and rows"""
    
    def convert(r):
        if isinstance(r, unicode):
            return r.encode("utf8")
        return r
    
    with open(name, "w") as f:
        writer = csv.writer(f)
        writer.writerow(columns)
        
        for row in rows:
            writer.writerow([convert(r) for r in row])

for shop in Shop.query.all():
    if not shop.shopify_token or shop.state == "uninstalled":
    #if shop.id != 30:
        continue
    
    prefix = shop.service_key.split("-")[0]
    print prefix
    
    try:
        open_shop(shop)
        
        info = shopify.Shop.current()
        with open(prefix + "_info.json", "w") as f:
            json.dump(info.attributes, f)
        
        
        #item,name,handle,price
        rows = []
        page = 1
        while True:
            products = shopify.Product.find(limit=250, page=page)
            
            for prod in products:
                prices = [v.price for v in prod.variants]
                price = sum([float(p) for p in prices])/len(prices)
                
                data = {'event': 'product-read-script', 'product': str(prod.id),
                    'variants': {str(var.id): {"price": var.price, "compare_at_price": var.compare_at_price} for var in prod.variants }}
                
                logstash_logger.info("", {'data': data})
                
                rows.append((prod.id, prod.title, prod.handle, price))
            
            if len(products) < 250: #if we got less then we asked for, this must be the last page
                break
            else:
                page += 1
        
        write_csv(prefix + "_items.csv", ["item","name","handle","price"], rows) #maybe we want to optimize discount too.
        
        
        
        #timestamp,visitor,cart,item,price,quantity,code
        rows = []
        page = 1
        while page < 10: #let's not get too many orders. just in case
            orders = shopify.Order.find(limit=250, page=page, status="any", financial_status="paid")
            for order in orders:
                if not hasattr(order, "customer"):
                    print order.created_at, "POS?"
                    continue
                
                time = datetime.strptime(order.created_at[:19], "%Y-%m-%dT%H:%M:%S")
                tz = order.created_at[19:]
                if tz:
                    time_offset = datetime.strptime(tz[1:], "%H:%M")
                    time_offset = timedelta(hours=time_offset.hour, minutes=time_offset.minute)
                    if tz[0] == "+":
                      time_offset *= -1
                    time += time_offset

                cart = order.cart_token #TODO: use this to find visitor? this would require a cart->visitor map built from logs...
                customer = order.customer.id #TODO: check if we can sometimes see this in js
                codes = order.discount_codes
                code = codes[0].code if len(codes) else ""
                
                for i in order.line_items:
                    rows.append((time.strftime("%Y-%m-%dT%H:%M:%S"), customer, cart, i.product_id, i.price, i.quantity, code, order.total_price, order.total_discounts))
            
            if len(orders) < 250: #if we got less then we asked for, this must be the last page
                break
            else:
                page += 1
        
        write_csv(prefix + "_purchases.csv", ["timestamp","visitor","cart","item","price","quantity","code", "total_order_revenue", "total_order_discount"], rows)
        
        close_shop()
          
    except Exception as e:
        print e
    

