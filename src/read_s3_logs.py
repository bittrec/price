from s3reader import *
from itertools import chain
from optserver.models import Shop

bucket = get_event_bucket()
logs = log_stream(bucket.list("ls.s3.optprice-optserver-"))

parsers = []
for shop in Shop.query.all():
    if shop.service_key == 'varlelt-services':
    #if shop.id != 25:
        continue
    
    prefix = shop.service_key.split("-")[0]
    
    parsers.append(TableParser(shop.id, prefix, is_shopify=shop.shopify_token is not None))
    parsers.append(PriceWatcher(shop.id, prefix))
    parsers.append(LogDumper(shop.id, prefix))

try:
    for_each_event(logs, parsers)
except KeyboardInterrupt:
    print "interrupting..."

for p in parsers:
    p.write()

