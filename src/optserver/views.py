__author__ = 'mumas'

import random
import traceback

from flask import render_template, render_template_string, request, g, make_response, send_from_directory

from core import logstash_logger
from optserver import app, health
from optserver.restapi import get_shops, get_shop_ids, get_is_shopify

from datetime import datetime, timedelta

def sanitize_id(id):
    try:
        return None if id in ("0", "-1", "", None) else int(id)
    except ValueError:
        return None

def get_shop(id, key):
   if key:
       id = get_shop_ids().get(key, None)
   else:
       id = sanitize_id(id)
       key = get_shops().get(id, "")
   return id, key

# -------
#  API  
# -------

cors_headers = {
    "Access-Control-Allow-Methods": "GET, POST",
    "Access-Control-Allow-Origin": "*",
    "Access-Control-Allow-Headers": "Content-Type, *"
}

@app.route('/inv')
@app.route('/<key>-services/inv')
def log_inventory(key=None):
    shop_id, key = get_shop(request.args.get('shop'), key)
    data = request.args.to_dict(flat=True)
    data["user-agent"] = request.user_agent.string
    data["referrer"] = request.referrer
    data["service-key"] = key
    data["shop"] = shop_id

    logstash_logger.info("", {'data': data})

    return "", 200, cors_headers

def make_response_with_cookie(*args):
    response = make_response(*args)
    
    if not request.cookies.has_key('bitrec-persistent-visitor-id'):
        response.set_cookie('bitrec-persistent-visitor-id', str(random.getrandbits(64)), expires=datetime(9999,1,1))#, TODO domain=".bitrec.com")
    
    return response

@app.route('/b.js')
@app.route('/<key>-services/b.js')
def b_script(key=None):
    shop_id, key = get_shop(request.args.get('shop'), key)
    
    if shop_id in get_is_shopify():
        template = "b-shopify.js"
    else:
        template = "b-" + key + ".js"

    #TODO: this really shouldn't be a "render_template".
    return make_response_with_cookie(render_template([template, "b.js"], key=key, persistentId=request.cookies.get('bitrec-persistent-visitor-id')),
                                     200, {'Content-Type': 'application/javascript; charset=utf8'})

if not app.config.get('SHOW_ERRORS'):
    @app.errorhandler(Exception)
    def all_exception_handler(e):
        app.logger.error('Unexpected exception: ' + repr(e))
        app.logger.error(traceback.format_exc())
        return '', 200

@app.route('/.well-known/acme-challenge/<path>')
def letsencrypt(path='WRONG'):
	return send_from_directory('well-known/acme-challenge/', path, mimetype='text/plain')	

#### HEALTH CHECKS ###
import time

currentTimeMillis = lambda: int(round(time.time() * 1000))


def service_available():
    return True, app.config['SERVICE_URL_KEY']


def database_available():
    start = currentTimeMillis()
    itemCount = "derp!"  # Item.query.count()
    discountCodeCount = "burp!"  # DiscountCode.query.count()
    diff = currentTimeMillis() - start
    return True, "DB is online; items: " + str(itemCount) + ", discountCodes: " + str(discountCodeCount) + " (" + str(diff) + "ms)"


# /healthcheck
health.add_check(service_available)
health.add_check(database_available)
