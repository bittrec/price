(function () {

function bitrecSend (url, script) {
    var base = 'https://foobar.bitrec.com/{{key}}-services';
    
    if (null == url) return
    
    url += "&r=" + (1+Math.random())
    {% if persistentId %}url += "&persistentId={{persistentId}}"{% endif %}
    
    if (script) {
        var s = document.createElement('script')
        s.type = 'text/javascript'
        s.async = true
        s.src = base + url
        var x = document.getElementsByTagName('script')[0]
        x.parentNode.insertBefore(s, x)
    
    } else {
        var xhr = new XMLHttpRequest();
        xhr.onreadystatechange = function() {}
        xhr.open("GET", base+url, true);
        xhr.send(null);
    }
}

function interceptAjaxRequests () {
    var realSend = XMLHttpRequest.prototype.send
    XMLHttpRequest.prototype.send = function () {
        var self = this;
        try {
            if (self.addEventListener)
                self.addEventListener("readystatechange", function() {
                    if (self.readyState === 4) parseAjaxResponse(self)
                })
            else {
                var f = self.onreadystatechange;
                self.onreadystatechange = function() {
                    if (f) f()
                    if (self.readyState === 4) parseAjaxResponse(self)
                }
            }
        } catch (e) {}
        
        realSend.apply(self, arguments)
    }
}

function bitrecGetCookie(name) {
    var value = "; " + document.cookie;
    var parts = value.split("; " + name + "=");
    if (parts.length >= 2) return parts.pop().split(";").shift();
}

function getVisitor() {
    var visitor = bitrecGetCookie("bitrec-visitor-id")// Math.random()
    if (!visitor) {
        visitor = Math.random()
        document.cookie = "bitrec-visitor-id="+visitor+"; expires=Fri, 01 Jan 9999 00:00:00 GMT; path=/"
    }
    return visitor
}

function parseProduct() {
    var path = window.location.pathname.split("/")
    var visitor = getVisitor()
    var cart = bitrecGetCookie("cart") || ""
    var product_id = __st.rid
    
    if (path[1] == "collections") {
      var collection = path[2]
      var handle = path[4]
    } else {
      var collection = ""
      var handle = path[2]
    }
    
    var url = "/inv"
    url = url + "?item=" + encodeURIComponent(product_id) + "&visitor=" + encodeURIComponent(visitor) +
         "&collection=" + encodeURIComponent(collection) + "&cart=" + encodeURIComponent(cart) +
         "&prevReferrer=" + encodeURIComponent(document.referrer) + "&fmt=click-item-shopify"
    
    return url
}

function parseAjaxResponse (r) {
    if (/cart\/add.js/.test(r.responseURL)) {
        var visitor = getVisitor()
        var cart = bitrecGetCookie("cart") || ""
        
        bitrecSend("/inv?visitor=" + encodeURIComponent(visitor) + "&cart=" + encodeURIComponent(cart) + "&fmt=add-to-cart-shopify")
    }
}

function parseCartPage() {
    var visitor = getVisitor()
    var cart = bitrecGetCookie("cart") || ""
    
    bitrecSend("/inv?&visitor=" + encodeURIComponent(visitor) + "&cart=" + encodeURIComponent(cart) +
         "&prevReferrer=" + encodeURIComponent(document.referrer) + "&fmt=shopify-cart")
}

function parsePage() {
    var visitor = getVisitor()
    var cart = bitrecGetCookie("cart") || ""
    
    bitrecSend("/inv?visitor=" + encodeURIComponent(visitor) + "&cart=" + encodeURIComponent(cart) +
         "&prevReferrer=" + encodeURIComponent(document.referrer) + "&fmt=click-any-shopify")
}

function afterLoaded(f) {
    if (document.readyState == "complete") f()
    else window.addEventListener("load", f)
}

afterLoaded(function () {
    try {
        if (window.location.pathname.indexOf("/products/") != -1)
            bitrecSend(parseProduct())
        
        else if (window.location.pathname.indexOf("/cart") != -1)
            parseCartPage()
        
        else parsePage()
        
        interceptAjaxRequests()
        
    } catch (e) {
        bitrecSend("/inv?event=error&message=" + encodeURIComponent(e.toString()) + "&stack=" + encodeURIComponent(e.stack || null), true)
    }
})

})();


