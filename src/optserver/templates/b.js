(function () {

function bitrecSend (url, script) {
    var base = 'https://foobar.bitrec.com/{{key}}-services';
    
    if (null == url) return
    
    url += "&r=" + (1+Math.random())
    {% if persistentId %}url += "&persistentId={{persistentId}}"{% endif %}
    
    if (script) {
        var s = document.createElement('script')
        s.type = 'text/javascript'
        s.async = true
        s.src = base + url
        var x = document.getElementsByTagName('script')[0]
        x.parentNode.insertBefore(s, x)
    
    } else {
        var xhr = new XMLHttpRequest();
        xhr.onreadystatechange = function() {}
        xhr.open("GET", base+url, true);
        xhr.send(null);
    }
}

function bitrecGetCookie(name) {
    var value = "; " + document.cookie;
    var parts = value.split("; " + name + "=");
    if (parts.length >= 2) return parts.pop().split(";").shift();
}

function getVisitor() {
    var visitor = bitrecGetCookie("bitrec-visitor-id")// Math.random()
    if (!visitor) {
        visitor = Math.random()
        document.cookie = "bitrec-visitor-id="+visitor+"; expires=Fri, 01 Jan 9999 00:00:00 GMT; path=/"
    }
    return visitor
}

function parsePage() {
    var visitor = getVisitor()
    
    bitrecSend("/inv?visitor=" + encodeURIComponent(visitor) + 
         "&prevReferrer=" + encodeURIComponent(document.referrer) + "&fmt=click-any")
}

function afterLoaded(f) {
    if (document.readyState == "complete") f()
    else window.addEventListener("load", f)
}

afterLoaded(function () {
    try {
        parsePage()
        
    } catch (e) {
        bitrecSend("/inv?event=error&message=" + encodeURIComponent(e.toString()) + "&stack=" + encodeURIComponent(e.stack || null), true)
    }
})

})();


