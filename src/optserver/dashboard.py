__author__ = 'mumas'

from optserver import app, health, login_manager, APP_URL
from optserver.restapi import get_shops, get_charts
from optserver.models import *
from optserver.views import sanitize_id
from optserver.core import logstash_logger, change_price

from flask.ext.login import login_user, logout_user, current_user, login_required
from flask import render_template, flash, request, g, make_response, redirect, url_for
from werkzeug import secure_filename

import traceback
import time
import random
from datetime import datetime
import requests
import json

### import boto

def post_to_slack(text):
  requests.post("https://hooks.slack.com/services/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",
                data={"payload":json.dumps({"channel":"#optprice_activity", "username":"priceopt app", "text": text})})

@app.route('/log')
def logging():
    post_to_slack(request.args.get("message"))
    return '', 200

def default_limits():
    return {"lower": -5, "upper": +50}

@app.route('/index')
@app.route('/')
@login_required
def index():
    """renders dashboard index html page with all campaigns, coupons and codes
    for this shop, embedded in script tags."""

    shop_id = g.user.shop
    shop = Shop.query.filter_by(id=shop_id).first()

    if shop.state == "uninstalled":
        return redirect(url_for("logout"))
 
    if shop.shopify_token is not None:
        try:
            open_shop(shop)
            collections = [{"id": col.id, "name": col.title, "lower": -5, "upper": +50} for col in shopify.CustomCollection.find()]
            close_shop()
        except:
	    #TODO: log the exception. maybe logging out would make sense too.
            collections = []
        
    else:
        collections = []
    
    collections = [{"id": "all", "name": "All Products", "lower": -5, "upper": +50}] + collections
    
    limits = Limits.query.filter_by(shop=shop_id).all()
    
    for c in collections:
        for l in limits:
            if l.collection == str(c["id"]):
                c["lower"] = l.lower
                c["upper"] = l.upper
    
    html = render_template("index.html", shop=shop, user=g.user, collections=collections)
    return html

@app.route('/set_state/<state>', methods=["POST"])
@login_required
def set_state(state):
    shop_id = g.user.shop
    shop = Shop.query.filter_by(id=shop_id).first()
    
    if shop.state == "off" and state == "on" or shop.state == "on" and state == "off":
        shop.state = state
    
    db.session.commit()
    
    if state == "off":
        rollback(shop)
    
    return '', 200

def rollback(shop):
    item_prices = {}
    for pr in Pricing.query.filter_by(shop=shop.id).order_by(Pricing.timestamp.asc()).all():
        key = (pr.product, pr.variant)
        ls = item_prices.get(key)
        if ls is None:
            ls = item_prices[key] = []
        ls.append(pr)
    
    for (prod, var), ls in item_prices.items():
        their_price = ([None] + [l for l in ls if l.model is None])[-1]
        our_price = ([None] + [l for l in ls if l.model is not None])[-1]
        if our_price is None or their_price is None: #if there are no modifications or no data at all in db, do nothing (this shouldn't happen)
            continue
        if our_price.timestamp < their_price.timestamp: #if the last price change is theirs, do nothing
            continue
        
        change_price(shop, prod, var, their_price.price, model='rollback') #TODO: this does open shop, commit to db, close shop. having it in a loop is silly 
        

@app.route('/set_limit', methods=["POST"])
@login_required
def set_limit():
    shop_id = g.user.shop
    shop = Shop.query.filter_by(id=shop_id).first()

    def to_number(s):
        try:
            return float(s)
        except:
            return None 
    
    collection = request.args.get('collection')
    lower = to_number(request.args.get('lower'))
    upper = to_number(request.args.get('upper'))

    lim = Limits.query.filter_by(shop=shop_id, collection=collection).first()
    if lim is None:
        lim = {"shop": shop_id, "collection": collection}
        lim.update(default_limits())
        lim = Limits(**lim)
        db.session.add(lim)

    if lower is not None:
        lim.lower = lower
    
    if upper is not None:
        lim.upper = upper

    db.session.commit()
    
    return '', 200


def parse_time(t):
    """Given a date string from JS, retuns a datetime or none"""
    try:
        return datetime.strptime(t, "%Y-%m-%dT%H:%M:%S.%fZ")
    except (ValueError, TypeError):
        return None


import os.path
#from boto.s3.connection import S3Connection, OrdinaryCallingFormat, ProtocolIndependentOrdinaryCallingFormat
import ssl

old_match_hostname = ssl.match_hostname


def new_match_hostname(cert, hostname):
    pos = hostname.find('.s3.amazonaws.com')
    if pos > -1:
        hostname = hostname[:pos].replace('.', '') + hostname[pos:]
    return old_match_hostname(cert, hostname)


ssl.match_hostname = new_match_hostname




@app.route('/settings', methods=["POST"])
def save_shop_settings():
    shop_id = g.user.shop
    
    shop = Shop.query.get(shop_id)
    
    shop.settings = json.dumps(request.json) #or just request.data or content or whatever?
    
    db.session.commit()
    
    #print "saving op ps", shop_id, op
    
    return "", 200


@app.route('/update_item', methods=["POST"])
def update_item():
    shop_id = sanitize_id(request.args.get('shop_id'))
    shop = Shop.query.get(shop_id)
    
    product = request.json
    #data = {'event': 'product-update-webhook', 'product': str(product["id"]),
    #    'variants': {str(var["id"]): {"price": var.get("price"), "compare_at_price": var.get("compare_at_price")} for var in product["variants"] }}
    #print "updating item", data
    
    #logstash_logger.info("", {'data': data}) #XXX no point in logging to logstash, if I'm going to log to db anyway
    
    for var in product["variants"]:
        p0 = Pricing.query.filter_by(shop=shop.id, product=str(product["id"]), variant=str(var["id"])).order_by(Pricing.timestamp.desc()).first()
        p = Pricing(timestamp=datetime.utcnow(),
                    shop=shop.id, product=str(product["id"]), variant=str(var["id"]),
                    price=float(var["price"]), model=None)
        
        if p0 is not None and p0.price == p.price:
            pass
        else:
            db.session.add(p)
    
    db.session.commit()
    
    return ''



from forms import LoginForm
import hashlib

def login_common(user):
    first_login = user.last_logged_in is None

    login_user(user)
    
    user.last_logged_in = datetime.utcnow()
    db.session.commit()
    
    return redirect(request.args.get("next") or url_for("index"))

@app.route("/login", methods=["GET", "POST"], endpoint="login")
def login():
    if g.user is not None and g.user.is_authenticated:
        return redirect(url_for('index'))

    form = LoginForm()

    if form.validate_on_submit():
        # login and validate the user...
        name, password = form.username._value(), hashlib.md5(form.password._value()).hexdigest()
        
        # look for user-name, user-password matches
        user = User.query.filter_by(name=name, password=password).first()
        
        if user:
            return login_common(user)
        
        else:
            # otherwise, check if it's shop-name/shop-services/shop-url, user1-password 
            shop = Shop.query.filter((Shop.name == name) | (Shop.service_key == name) | (Shop.service_key == name + "-services") | (Shop.service_key == name + "myshopifycom-services") | (Shop.url == name) | (Shop.url == name + ".myshopify.com")).first()
            
            user = User.query.get(1) #user with id = 1 will be our admin
            
            if shop and user:
                user.name = "bitrec " + shop.url
                user.shop = shop.id #XXX: we need to set shop id since we use it in dashboard. login_common will do the commit for us
                
                return login_common(user)
            
            else:
                flash('Wrong email or password')
                return render_template("login.html", form=form)
        
    return render_template("login.html", form=form)

import shopify
from optserver import API_SCOPE


@app.route("/login/shopify", methods=["GET", "POST"])
def login_with_shopify():
    #if g.user is not None and g.user.is_authenticated:
    #    return redirect(url_for('index'))
    
    form = LoginForm()
    
    shop_url = request.args.get('shop') or form.username._value()
    
    if shop_url:
        ss = shopify.Session(shop_url.strip())
        permission_url = ss.create_permission_url(API_SCOPE, 
                                       url_for("install_shopify", _external=True))
        return redirect(permission_url)

    return render_template("shopify_login.html", form=form)

import re
from optserver import open_shop, close_shop

def make_shopify_service_key(shop_url):
    return re.sub("[^a-zA-Z0-9]", "", shop_url) + "-services" #TODO: check for duplicates 

@app.route('/login/shopify/finalize')
def install_shopify():
    """
    Receives shop parameters from shopify and creates Shop db row and cookies.
    Serves both for installing and logging in.
    URL of this endpoint needs to be given in shopify app configuaration.
    """
    shop_url = request.args.get('shop')
    token = shopify.Session(shop_url).request_token(request.args)
    
    print "start installing", shop_url, "at", datetime.now()
    
    if not token:
        raise Exception('no token')
    
    shop = Shop.query.filter_by(url=shop_url).first()
    user = User.query.filter_by(name=shop_url).first()
    
    if not shop or shop.shopify_token != token: #guessing that token will only change on reinstall 
        if not shop:
            shop = Shop(name=shop_url, url=shop_url, shopify_token=token,
                        service_key=make_shopify_service_key(shop_url), state="off")
            db.session.add(shop)
        else:
            shop.shopify_token = token
            shop.state = "off"
        
        if not user:
            user = User(name=shop_url, password="", shop_ref=shop)
            db.session.add(user)
        
        open_shop(shop)
        
        email = shopify.Shop.current().email
        shop.email = email
        
        db.session.commit()
        
        SCRIPT_SRC = APP_URL + shop.service_key + "/b.js"
        script = shopify.ScriptTag.find(src=SCRIPT_SRC)
        if not script:
            r = shopify.ScriptTag({'src': SCRIPT_SRC, 'event': 'onload'}).save() 
            print "adding script tag", SCRIPT_SRC, r

        post_to_slack(shop_url + " installed priceopt")
        
        #if email:
        #    mail.send(mail.welcome_email, email, shop_url)
        
        UNINSTALL_URL = APP_URL[:-1] + url_for('uninstall_shopify') + "?shop_id=" + str(shop.id)
        shopify.Webhook({'address': UNINSTALL_URL, 'topic': 'app/uninstalled'}).save()
        print "added app/uninstalled hook to", UNINSTALL_URL
        
        UPDATE_ITEM_URL = APP_URL[:-1] + url_for('update_item') + "?shop_id=" + str(shop.id)
        shopify.Webhook({'address': UPDATE_ITEM_URL, 'topic': 'products/create'}).save()
        shopify.Webhook({'address': UPDATE_ITEM_URL, 'topic': 'products/update'}).save()
        print "added products/create.update hook to", UPDATE_ITEM_URL
        
        
        #charge = shopify.RecurringApplicationCharge({
        #      'name': 'basic plan', 'terms': 'terms: 2% of sales generated by optprice',
        #      'price': '0', 'capped_amount': '1000', 'trial_days': '14',
        #      'test': True if shop.url == 'bananabread.myshopify.com' else None,
        #      'return_url': APP_URL + 'shopify/process_charge?shop_id={0}&user_id={1}'.format(shop.id, user.id)
        #    })
        #charge.save()
        
        close_shop()
        
        print "finished installing", shop_url, "at", datetime.now()
        
        #print "installed", shop_url, token
        #return redirect(charge.confirmation_url)
    
    return login_common(user)

@app.route('/shopify/uninstall', methods=["GET", "POST"])
def uninstall_shopify():
    shop_id = sanitize_id(request.args.get('shop_id'))
    
    shop = Shop.query.get(shop_id)
    if shop.state == "uninstalled":
        post_to_slack(shop.url + " called uninstall while not installed?")
        return ''
    
    shop.state = "uninstalled"
    db.session.commit()
    
    post_to_slack(shop.url + " uninstalled optprice")
    
    #if shop.email:
    #    mail.send(mail.goodbye_email, shop.email, shop.url)
      
    print "uninstalling", shop.url
    return ''

@app.route('/shopify/process_charge')
def shopify_billing():
    charge_id = request.args.get('charge_id')
    shop_id = sanitize_id(request.args.get('shop_id'))
    user_id = sanitize_id(request.args.get('user_id'))
    
    print "got", charge_id, shop_id, user_id
    
    shop = Shop.query.get(shop_id)
    user = User.query.get(user_id)
    
    open_shop(shop)
    
    charge = shopify.RecurringApplicationCharge.find(charge_id)
    
    print "stat", charge.status
    
    if charge.status == "accepted":
        event = "shop " + shop.url + " accepted our charge " + str(charge.id)
        print event
        post_to_slack(event)
        charge.activate()
    
    elif charge.status == "declined":
        event = "shop " + shop.url + " declined our charge " + str(charge.id)
        print event
        post_to_slack(event)
        #TODO: what do we do now? uninstall? wait for end of trial and aska again?
    
    else:
        event = "shop " + shop.url + " did something to our charge " + str(charge.id) + " and now it's " + str(charge.status)
        print event
        post_to_slack(event)
        #TODO: can state even be different
    
    close_shop()
    
    return login_common(user)

@app.route('/logout', endpoint="logout")
@login_required
def logout():
    user = g.user
    if user is not None:
      shop = Shop.query.get(user.shop)
      logout_user()
      
      if shop and shop.shopify_token and shop.shopify_token is not None:
        return redirect('http://' + shop.url + '/admin/apps')
    
    return redirect(url_for('index'))


@login_manager.user_loader
def load_user(id):
    return User.query.get(int(id))


@app.before_request
def before_request():
    g.user = current_user
