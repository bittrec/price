import flask
from flask import abort, request
from optserver import auth, verify_password
from flask_admin import Admin
from flask_admin.contrib.sqla import ModelView
import datetime
from sqlalchemy import Column, String, Integer, Boolean, Float

from optserver import db, app

class Shop(db.Model):
    id = Column(Integer, primary_key=True)
    name = Column(String(255))
    url = Column(String(2048))
    email = Column(String(255))
    service_key = Column(String(255))
    shopify_token = db.Column(db.String(512))
    #model = Column(Integer, db.ForeignKey('model.id'))
    
    state = Column(String(255)) # 'on' | 'off' | 'uninstalled'. '' means 'off' too
    settings = Column(db.Text)
    
    def __str__(self):
        return self.name

class User(db.Model):
    id = Column(Integer, primary_key=True)
    name = Column(String(255))
    password = Column(String(255))
    shop = Column(Integer, db.ForeignKey(Shop.id))
    shop_ref = db.relationship(Shop)
    last_logged_in = Column(db.DateTime)
    ask_for_feedback = Column(Boolean())
    
    def is_authenticated(self):
        return True

    def is_active(self):
        return True

    def is_anonymous(self):
        return False

    def is_super(self):
        return "bitrec" == self.name

    def get_id(self):
        try:
            return unicode(self.id)  # python 2
        except NameError:
            return str(self.id)  # python 3

    def __repr__(self):
        return self.name

class Limits(db.Model):
    shop = Column(Integer, primary_key=True)
    collection = Column(String(255), primary_key=True) #shopify collection id, stringified
    lower = Column(Float)
    upper = Column(Float)
    
    def __str__(self):
        return str((self.shop, self.collection, self.lower, self.upper))

#TODO: we might need a price history table (because logs aren't real time enough, for "rollback" button to work, and maybe any delay is a problem for regular update script too)

class Pricing(db.Model):
    id = Column(Integer, primary_key=True)
    timestamp = Column(db.DateTime, nullable=False)
    shop = Column(Integer, nullable=False)
    product = Column(String(255), nullable=False) #shopify product id, string because int is not long enough
    variant = Column(String(255), nullable=False) #shopify variant id, stringified also
    price = Column(Float, nullable=False)
    model = Column(String(255)) #the model which computed the price. NULL means this price was set by shop owner manually
    
    def __str__(self):
        return str((self.timestamp, self.shop, self.product, self.variant, self.price, self.model))


# Create customized authenticated model view class
class AuthenticatedModelView(ModelView):
    
    column_exclude_list = ('popup', 'data') #FIXME: these options work neither on Model classes nor added to the view instance
    form_excluded_columns = ('data', )
    
    @auth.login_required
    def is_accessible(self):
        auth = request.authorization
        return auth and verify_password(auth.username, auth.password)

    @auth.login_required
    def _handle_view(self, name, **kwargs):
        """
        Override builtin _handle_view in order to redirect users when a view is not accessible.
        """
        if not self.is_accessible():
            abort(403)

admin = Admin(app, template_mode='bootstrap3')
for entity in [Shop, User]:
    view = AuthenticatedModelView(entity, db.session)
    admin.add_view(view)

