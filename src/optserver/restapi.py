"""
We do REST API using two libraries: restfull and restless.
Restless is used for automatic API for models, and restufll is used for the main discount API.
"""

import threading
import time
import flask
import pickle
from datetime import datetime, timedelta

from flask_restful import reqparse, abort, Resource

from optserver import api, app, restmanager, db
from models import Shop

import requests

global_cache = {}

def get_charts(refresh=False, key_prefix='get_charts'):
    """ask analytics.bitrec.com for the latest discount chart.
    
    XXX this is for dahsboard ONLY and might need to be moved, should we want to
    separate discount serving from configuration."""
    
    return {} #XXX: since we don't render charts yet, we might as well not query them
    
    if refresh or global_cache.get(key_prefix, None) is None:
        charts = {}
        for shop_id in app.config['SHOPS']:
            try:
                r = requests.get("http://analytics.bitrec.com/dump/opt/" + str(shop_id) + "?columns=cart-count,session-count,cart-rec-count-C-bucket-ctrl,cart-rec-count-C-bucket,cart-count-C-bucket-ctrl,cart-count-C-bucket,total-revenue,recommendation-revenue-C-bucket,used-sum")
                
                data = r.json()
                
                if not data:
                    app.logger.info('Empty chart for shop {}.'.format(shop_id))
                
                #print shop_id, data
                
            except Exception, e:
                app.logger.info('Couldn\'t load chart for shop {}.'.format(shop_id))
                print e
                data = None
            
            charts[shop_id] = data
            
        app.logger.info('Reloaded charts.')
        
        global_cache[key_prefix] = charts
    
    return global_cache[key_prefix]




def get_shops(refresh=False, key_prefix='get_shops'):
    """load all the shops for current machine into app.config['SHOPS']
    
    then make a map of shop id -> service key"""
    if refresh or global_cache.get(key_prefix, None) is None:
        
        if len(app.config.get('SHOPS', [])) == 0:
            shops = Shop.query.all() #TODO: do we load uninstalled shops? some of them still have our script. so we can collect some data.
            if 'SHOP_PREDICATE' in app.config:
                shops = [s for s in shops if app.config['SHOP_PREDICATE'](s)]
        
        else:
            shops = [Shop.query.get(i) for i in app.config['SHOPS']]
            shops = [s for s in shops if s is not None]
        
        app.config['SHOPS'] = [s.id for s in shops]
        
        names = {s.id: s.service_key.split("-")[0] for s in shops}
        ids = {key: id for id, key in names.items()}
        is_shopify = set([s.id for s in shops if s.shopify_token is not None])
        app.logger.info('Reloaded shops.\n{}'.format(names))
        
        global_cache[key_prefix] = names
        global_cache["shop_ids"] = ids
        global_cache["is_shopify"] = is_shopify
    
    return global_cache[key_prefix]

def get_shop_ids():
    return global_cache["shop_ids"]

def get_is_shopify():
    return global_cache["is_shopify"]


def refresh_data():
    """
    If you see the data loaded on each request then your WSGI configuration is wrong;
    It is Apache that creates a new process for each request. Adjust your WSGI setup to use daemon mode
    """
    try:
        import uwsgi
        app.logger.info("Refreshing data for worker {}".format(uwsgi.worker_id()))
    except:
        pass
    
    get_shops(True)

    get_charts(True)


