import random
import logging
import redis
import time
from hash_ring import HashRing
from datetime import datetime
from optserver import *
from logging.handlers import RotatingFileHandler
import logstash

from logstash_formatter import LogstashFormatterV1

from optserver.models import Pricing

logstash_logger = logging.getLogger()
formatter = LogstashFormatterV1()

handler = RotatingFileHandler(app.config['LOGSTASH_LOGGING_FILE'], maxBytes=app.config['LOGSTASH_MAX_SIZE'], backupCount=app.config['LOGSTASH_FILE_COUNT'])
handler.setFormatter(formatter)
logstash_logger.addHandler(handler)

handler = logstash.LogstashHandler('127.0.0.1', 8170)
handler.setFormatter(formatter)
logstash_logger.addHandler(handler)

logstash_logger.setLevel(logging.INFO)


def change_price(shop, product_id, variant_id, new_price, model='?'):
    open_shop(shop)
    
    try:
        v = shopify.Variant.find(variant_id)
        
        old_price = float(v.price) #v.price is a string
        v.price = new_price #but it's okay to set it to a float
        
        if not v.save():
            print "save variant", variant_id, "returned False"
        else:
            p = Pricing(timestamp=datetime.utcnow(),
                        shop=shop.id, product=product_id, variant=variant_id,
                        price=float(v.price), model=model)
            db.session.add(p)
            db.session.commit()
        
    except Exception as e: #TODO catch pyactiveresource.connection.ResourceNotFound, maybe also pyactiveresource.connection.BadRequest
        print "while changing price for variant", variant_id, "to", new_price, "got error", repr(e)
    
    close_shop()




