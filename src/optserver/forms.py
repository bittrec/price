from flask_wtf import Form
from wtforms import BooleanField, PasswordField, StringField
from wtforms.validators import DataRequired, Email, Length, InputRequired

class LoginForm(Form):
    username = StringField('Username', validators=[InputRequired(),  Length(min=0, max=255)])
    password = PasswordField('Password', [DataRequired(), Length(min=0, max=255)])
    remember_me = BooleanField('remember_me', default=False)