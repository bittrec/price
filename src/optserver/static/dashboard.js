angular.module("nn", ["angularUtils.directives.dirPagination", "ui.bootstrap", "angular-confirm", 'ngSanitize', 'uiSwitch'])

  .controller("body", function ($scope, $http, $location, $timeout, $sce) {
    $scope.page = "dashboard"
    $scope.user = USER
    $scope.shop = SHOP
    $scope.collections = COLLECTIONS
    var img_base = $scope.img_base = IMG_BASE

    $scope.to_percent = function (f) {
      if (f !== undefined) return Math.floor(f * 100000) / 1000.0
    }

    $(window).on("resize", function () {
      console.log("R")
      $scope.$apply()
    })
    
    
    // feedback popup. states are 'rate_us', 'review_us', 'give_feedback' and null
    
    $scope.feedback = {state: null, rating: 0}
    
    $scope.log_to_backend = function (msg) {
      return $http({method: "GET", url: "/log", params: {"message": "from " + SHOP.url + "(" + SHOP.id + ")\n" + msg}})
    }
    
    if (USER.ask_for_feedback) $timeout(function () {
      $scope.feedback.state = 'rate_us'
    }, 10000)
    
    
    // shop settings
    
    var op_save_timeout = undefined
    
    $scope.saveShopSettings = function () {
      if (op_save_timeout) $timeout.cancel(op_save_timeout)
      op_save_timeout = $timeout(function () {
          //console.log("saving op ps", $scope.shop.optimization_priorities)
          $http({method: "POST", url: "/settings", data: $scope.shop.settings})
          op_save_timeout = null
      }, 1000)
    }

    // DIR-PAGINATE

    $scope.pagination = {
        currentPage: 1,
        itemsPerPage: 5
    }

    $scope.pageIndex = function($index) {
        return $scope.pagination.itemsPerPage * ($scope.pagination.currentPage - 1) + $index;
    }

    $scope.sort = function (keyname) {
      $scope.sortKey = keyname;   //set the sortKey to the param passed
      $scope.reverse = !$scope.reverse; //if true make it false and vice versa
    }

    // IO
    
    $scope.setLimit = function (collection) {
      $http({method: "POST", url: "/set_limit", params: {collection: collection.id, upper: collection.upper, lower: collection.lower}})
    }
    
    $scope.setState = function (state) {
      $http({method: "POST", url: "/set_state/" + state}).then(function (res) {
          $scope.shop.state = state
      })
    }

    // ROUTING

    // validates ids parsed from the location url and sets global variables
    /*var open_campaign_page = function (page, campaign_id, coupon_id) {
        if (_.isString(page)) page = {page: page, wizard: false}

        if (!$scope.campaign || //no campaign selected, OR
            !$scope.campaign.id && !_.isNaN(campaign_id) || //selected campaign has no id, but it should
            $scope.campaign.id && $scope.campaign.id != campaign_id) { //selected campaign has id and it is wrong
            if (_.isNaN(campaign_id))
                var c = {name: "New campaign", coupons: []} //template for a new campaign. with current layout, it probably shouldn't have default a coupon.
            else
                var c = _.find($scope.campaigns, function (c) {
                                return c.id == campaign_id
                            })

            if (!c) return false
            else
                $scope.campaign = c
        }

        if ($scope.campaign && coupon_id !== null && coupon_id !== undefined) {
            if (!$scope.coupon || //no coupon selected, OR
                !$scope.coupon.id && !_.isNaN(coupon_id) || //selected coupon has no id, but it should
                $scope.coupon.id && $scope.coupon.id != coupon_id) { //selected coupon has id and it is wrong

                if ($scope.campaign.coupons.length > 0) {
                  var c = $scope.campaign.coupons[0]
                } else {
                  var c = $scope.getDefaultCoupon()
                }

                if (!c) return false
                else {
                    $scope.coupon = c
                }
            }
        } else $scope.coupon = undefined

        $scope.page = page.page
        $scope.wizard = page.wizard
        return true
    }

    // given global page, campaign and coupon values, sets location.path such that open_campaign_page would find these same campaign and coupon
    var go_to_page = function (page, campaign_id, coupon_id) {
        var url = null
        if (_.isString(page)) page = {page: page, wizard: false}
        if (!page.page) page.page = $scope.page

        campaign_id = page.campaign_id || campaign_id
        coupon_id = page.coupon_id || coupon_id

        if (page.page == "campaign-list") url = "/campaigns"

        if (!page.wizard) {
            //if (page.page == "campaign-settings") url = "/campaign/"+campaign_id+"/settings"
            //if (page.page == "campaign-coupon-list") url = "/campaign/"+campaign_id+"/coupons"
            if (page.page == "campaign-coupon") url = "/campaign/" + campaign_id + "/coupons/" + coupon_id
            if (page.page == "campaign-popup") url = "/campaign/" + campaign_id + "/coupons/" + coupon_id + "/popup"
            if (page.page == "popup-preview") url = "/campaign/" + campaign_id + "/coupons/" + coupon_id + "/preview"

        } else { // wizard pages should probably include campaign/coupon ids as well
            //if (page.page == "campaign-settings") url = "/wizard/campaign/" + campaign_id + "/" + coupon_id
            //if (page.page == "campaign-coupon") url = "/wizard/coupon/" + campaign_id + "/" + coupon_id
            if (page.page == "campaign-popup") url = "/wizard/popup/" + campaign_id + "/" + coupon_id
            if (page.page == "popup-preview") url = "/wizard/preview/" + campaign_id + "/" + coupon_id
        }

        if (url !== null) $location.path(url)
    }

    $scope.$on("$locationChangeStart", function (ev, n, o) {
        path = n.split('#').slice(1).join('#')

        m = /^\/faq/.exec(path)
        if (m) $scope.page = "faq"

        console.log('page: '+ $scope.page)
    })*/
  })

  .directive('nnPercent', function () {
      return {
          require:'ngModel',
          restrict:'A',

          link: function (scope, elm, attrs, ctrl) {
              ctrl.$formatters.push(function (modelValue) {
                  return Math.floor(modelValue*100) || 0
              })

              ctrl.$parsers.push(function (viewValue) {
                  return Math.floor(Number(viewValue))/100 || 0
              })
          }
      }
  })

  .directive('nnScroll', function ($log) {
    return function ($scope, elem, attr) {
      elem.on("scroll", function () {
        if (!$scope.$$phase)
          $scope.$apply()
      })
    }
  })

  /* Directive for jQuery datepicker. */
  .directive('nnDatepicker', function (time) {
    return function ($scope, $elem, $attr) {
      var arg = $attr.nnDatepicker

      $elem.datepicker({
        prevText: "", nextText: "",
        minDate: time.T00,  maxDate: time.T11,
        defaultDate: time[arg],

        onSelect: function (date) {
          time[arg] = new Date(date)
          if (!$scope.$$phase)
            $scope.$apply()
        }
      })

      var dp = time.datepickers[arg] = {}

      dp.set = function (name, value) {
        $elem.datepicker("option", name, value)
      }

      dp.open = function () {
        $elem.datepicker("show")
        var right = $(window).width() - $elem.offset().left - 10
        $(".ui-datepicker").css({left: "auto", right: right})
      }
    }
  })

  .service('time', function ($rootScope) {
    var T = this



    this.round_to = {
      day: function (D) {
        d = new Date(D)
        d.setHours(0)
        d.setMinutes(0)
        return d
      },
      week: function (D) {
        d = T.round_to.day(D)
        d.setDate(d.getDate()-d.getDay()+1)
        return d
      }
    }

    this.fmt = d3.time.format("%d %b %Y")
    this.datepickers = {}
    this.open = function (name) {
      if (T.datepickers[name]) T.datepickers[name].open()
    }

    this.ROUND = "day"
    //this.T0 = this.T1 = this.T00 = this.T11 = null
    T.T0 = T.T00 = new Date("2016-03-01")
    T.T1 = T.T11 = new Date()

    $rootScope.time = this
    window.time = this

    /*this.save = function () {
      // TODO: if T.T1 == T.T11, then send T1 = null
      return $http.get("/save-filter?T0=" + T.T0.getTime() + "&T1=" + T.T1.getTime())
    }*/

    $rootScope.$watchCollection(function () { return [T.T0, T.T1, T.datepickers] }, function (ts) {
      if (!T.T0 || !T.T1) return

      if ((T.T1.getTime() - T.T0.getTime())/(1000*60*60*24) < 14) T.ROUND = "day"

      if (T.datepickers.T0) T.datepickers.T0.set("maxDate", T.T1)
      if (T.datepickers.T1) T.datepickers.T1.set("minDate", T.T0)
    }, true)
  })

  /* Directive for chart redrawing. Watches all relevant variables so a
     manual redraw shouldn't be needed. Uses the old redraw* functions. */
  .directive('chart', function (time) {
    var redrawLineChart = function (source, columns, format, chart) {
      var column_titles = _.map(columns, function (c) { return {title: c[0], index: c.index} })
      var data = makeData(source, columns)
      //console.log(data)
      chart.html("")
      drawLineChart(data, column_titles, format, chart)
    }

    var at = function (key) {
        return function (val) { return val[key] }
    }

    /* src is {param: [data], ..., index: [dates]},
   func is round_to.day or round_to.week
   returns same object, but with data grouped and index changed */
    function group(src, func) {
      var res = {}
      _.each(src, function (arr, key) {
        res[key] = []
      })

      _.each(src.index, function (d, i) {
        ix = func(d)
        is_new = !_.last(res.index) || _.last(res.index).getTime() != ix.getTime()
        if (is_new) res.index.push(ix)
        _.each(src, function (arr, key) {
          if (key != "index") {
            if (is_new) res[key].push(src[key][i])
            else res[key][res[key].length-1] += src[key][i]
          }
        })
      })

      return res
    }

    var makeData = function(source, columns) {
      source = group(source, time.round_to[time.ROUND])
      index = source["index"] = _.map(source["index"], function (d) { return new Date(d) })

      //recursively evaluates a function on data columns
      //  func = ["*", arr, num] or ["/", arr, arr] or ["min", arr, num]
      function calculate(func) {
        if (_.isString(func))
          return source[func]
        else if (func[0] == "*" && _.isNumber(func[2]))
          return _.map(calculate(func[1]), function (v) { return v*func[2] })
        else if (func[0] == "*") {
          var d = calculate(func[2])
          return _.map(calculate(func[1]), function (n, i) { return n*d[i] })
        } else if (func[0] == "+") {
          var d = calculate(func[2])
          return _.map(calculate(func[1]), function (n, i) { return n+d[i] })
        } else if (func[0] == "/") {
          var d = calculate(func[2])
          return _.map(calculate(func[1]), function (n, i) { return (d[i] != 0 && n/d[i]) || 0 })
        } else if (func[0] == "min" && _.isNumber(func[2]))
          return _.map(calculate(func[1]), function (v) { return Math.min(v, func[2]) })
        else if (func[0] == "min") {
          var d = calculate(func[2])
          return _.map(calculate(func[1]), function (n, i) { return Math.min(n, d[i]) })
        }
      }

      var data_columns = _.map(columns, function (c, i) {
        return calculate(c[1])
      })

      var data_rows = _.map(index, function (d, i) {
        return _.map(data_columns, at(i))
      })

      return _.filter(data_rows, function (r) {
        return r[0] >= time.T0 && r[0] <= time.T1
      })
    }

    var drawLineChart = function (data, columns, format, $element) {
      //set up chart size and margins
      var margin = {top: 30, left: 60, bottom: 40, right: 20}
      var width = $element.parent().width() - margin.left - margin.right
      var height = width * 0.3 //Math.min(Math.max($(window).height() - margin.top - margin.bottom - 290, 200), width)

      //find min/max of each column
      var data = _.map(data, function (row) {
        var tail = row.slice(1)
        row.min = d3.min(tail)
        row.max = d3.max(tail)
        return row
      })

      //creates a format function
      //  fmt = [prefix, format, suffix]
      //  func = d3.time.format of d3.format
      //  dashDate = true if weeks should be shown as intervals
      function formatter(fmt, func, dashDate) {
          if (!fmt || !fmt[1]) return
          var mid = func(fmt[1])
          if (dashDate === false) {
            mid = func("%b %d")
          }
          var base = function (n) { return fmt[0] + mid(n) + fmt[2]; }
          if (time.ROUND == "week" && dashDate)
            return function (n) {
              var m = new Date(n)
              m.setDate(n.getDate()+6)
              return base(n) + "-" + base(m)
            }
          else
          return base
      }

      //define scales and find ticks
      var x = d3.time.scale()
          .domain([data[0][0], data[data.length-1][0]])
          .range([margin.left, margin.left+width]);
      var ticksX = x.ticks(5)

      var tickMonths = _.map(ticksX, function (d) { return d.getMonth() })
      var sameMonth = _.any(tickMonths, function (m, i, ms) { return ms[i] == ms[i+1] })

      var fmtX = formatter(format.x[sameMonth ? "day": "month"], d3.time.format, false) || x.tickFormat(5)
      var fmtPopupX = formatter(format.x[time.ROUND], d3.time.format, true) || x.tickFormat(5)

      var y = d3.scale.linear()
          .domain([d3.min(data, at("min")) || format.min_range.min || 0, d3.max(data, at("max")) || format.min_range.max || 0])
          .range([margin.top+height, margin.top]).nice();
      var ticksY = y.ticks(5)

      if (ticksY.length > 1) {
        d = ticksY[ticksY.length-1] - ticksY[ticksY.length-2]
        if (ticksY[ticksY.length-1]+d*0.1 < d3.max(data, at("max"))) {
          y.domain([d3.min(data, at("min")), d3.max(data, at("max")) + d])
          ticksY = y.ticks(6)
        }
      }

      var fmtY = formatter(format.y, d3.format) || y.tickFormat(5)
      var fmtPopupY = formatter(format.y, d3.format) || y.tickFormat(5)

      //build chart area
      var chart = d3.select($element.get(0))
          .attr("width", margin.left + width + margin.right)
          .attr("height", margin.top + height + margin.bottom);


      //build Y axis
      var line = chart.selectAll("g.line")
          .data(ticksY)
        .enter().append("g").attr("class", "line")
          .attr("transform", function (t) { return "translate(" + 0 + ", " + y(t) + ")" })

      line.append("line")
          .attr("x1", 0).attr("x2", margin.left+width+margin.right)
          .attr("y1", 0).attr("y2", 0)

      line.append("text")
          .attr("x", 0)
          .attr("y", -10)
          .text(fmtY);

      //build X axis
      var date = chart.selectAll("g.date")
          .data(ticksX)
        .enter().append("g").attr("class", "date")
          .attr("transform", function (t) { return "translate(" + x(t) + ", " + (margin.top+height) + ")"})

      date.append("line")
          .attr("x1", 0).attr("x2", 0)
          .attr("y1", 0).attr("y2", y(ticksY[ticksY.length-1])-(margin.top+height))

      date.append("text")
          .attr("x", 0)
          .attr("y", 20)
          .text(fmtX);

      chart.append("line")
          .attr("x1", 0).attr("x2", margin.right+width+margin.left)
          .attr("y1", margin.top+height).attr("y2", margin.top+height)
          .attr("class", "bottom")

      //build lines
      _.each(columns, function (col, i) {
        if (i == 0) return;

        var cl = "c"+(col.index || i)

        var zigzag = d3.svg.line()
            .x(function (d) { return x(d[0]) })
            .y(function (d) { return y(d[i]) })

        chart.append("path").attr("class", cl).attr("d", zigzag(data))

        var circle = chart.selectAll("g.circle."+cl)
            .data(data)
          .enter().append("g").attr("class", "circle "+cl)
            .attr("transform", function(d) { return "translate(" + x(d[0]) + "," + y(d[i]) + ")"; });

        circle.append("circle")
            .attr("class", "outer")
            .attr("r", 20)

        _.each(circle[0], function (out, j) {
            var d = data[j]

            //built tooltip
            var tip = $("<div>").addClass("tooltip box gradient small")
            $("<span>").addClass("date").appendTo(tip).text(fmtPopupX(d[0]))
            $("<br>").appendTo(tip)
            $("<span>").addClass("title caps").appendTo(tip).text(col.title + ": ")
            $("<span>").addClass("medium").appendTo(tip).text(fmtPopupY(d[i]))

            $(out).on("mouseenter", function (ev) {
                $element.after(tip)
                tip.css({
                    top: y(d[i]) - 65,
                    right: $element.width() - x(d[0]) - 20  })
            })

            $(out).on("mouseleave", function (ev) {
                tip.remove()
            })
        })

        circle.append("circle")
            .attr("class", "inner")
            .attr("r", 4)
      })
    }

    return function ($scope, $elem, $attr) {
      var conf = $scope.$eval($attr.chart)
      var func = redrawLineChart //function (data, columns, format, el) { console.log("redrawing", data, columns, format, el) }

      var redraw = function () { if (conf) func(conf.data, conf.columns, conf.format, $elem) }
      redraw()

      $(window).on("resize", redraw)
      $scope.$watch(function () { return [$scope.time.T0, $scope.time.T1, $scope.time.ROUND, conf.columns] }, redraw, true)
    }
  })

  .filter('to_trusted', ['$sce', function($sce){
      return function(text) {
          return $sce.trustAsHtml(text);
      };
  }])

