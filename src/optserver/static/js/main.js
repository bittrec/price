
angular.module('nn', [])
  /* Makes angluar use [[..]] for interpolation, instead of the default {{...}} */
  .config(function($interpolateProvider) {
    $interpolateProvider.startSymbol('[[');
    $interpolateProvider.endSymbol(']]');
  })
  
  /* Holds time bounds (T0, T1, T00, T11), granularity and all datepickers.
     Can round and format dates, save intervals.  */
  .service('time', function ($rootScope, $http, testData) {
    var T = this
    
    this.round_to = {
      day: function (D) {
        d = new Date(D)
        d.setHours(0)
        d.setMinutes(0)
        return d
      },
      week: function (D) {
        d = T.round_to.day(D)
        d.setDate(d.getDate()-d.getDay()+1)
        return d
      }
    }
    
    this.fmt = d3.time.format("%d %b %Y")
    this.datepickers = {}
    this.open = function (name) {
      if (T.datepickers[name]) T.datepickers[name].open()
    }
    
    this.ROUND = "week"
    this.T0 = this.T1 = this.T00 = this.T11 = null
    
    $rootScope.time = this
    window.time = this
    
    this.save = function () {
      // TODO: if T.T1 == T.T11, then send T1 = null
      return $http.get("/save-filter?T0=" + T.T0.getTime() + "&T1=" + T.T1.getTime())
    }
    
    $rootScope.$watchCollection(function () { return [T.T0, T.T1, T.datepickers] }, function (ts) {
      if (!T.T0 || !T.T1) return
      
      if ((T.T1.getTime() - T.T0.getTime())/(1000*60*60*24) < 14) T.ROUND = "day"
      
      if (T.datepickers.T0) T.datepickers.T0.set("maxDate", T.T1)
      if (T.datepickers.T1) T.datepickers.T1.set("minDate", T.T0)
    }, true)
  })
  
  /* Directive for jQuery datepicker. */
  .directive('datepicker', function (time) {
    return function ($scope, $elem, $attr) {
      var arg = $attr.datepicker
      
      $elem.datepicker({
        prevText: "", nextText: "",
        minDate: time.T00,  maxDate: time.T11,
        defaultDate: time[arg],
        
        onSelect: function (date) {
          time[arg] = new Date(date)
          if (!$scope.$$phase)
            $scope.$apply()
        }
      })
      
      var dp = time.datepickers[arg] = {}
      
      dp.set = function (name, value) {
        $elem.datepicker("option", name, value)
      }
      
      dp.open = function () {
        $elem.datepicker("show")
        var right = $(window).width() - $elem.offset().left - 10
        $(".ui-datepicker").css({left: "auto", right: right})
      }
    }
  })
  
  /* Directive for chart redrawing. Watches all relevant variables so a
     manual redraw shouldn't be needed. Uses the old redraw* functions. */
  .directive('chart', function () {
    return function ($scope, $elem, $attr) {
      var conf = $scope.$eval($attr.chart)
      var func = {line: redrawLineChart, box: redrawBoxPlot}[conf.type]
      
      var redraw = function () { func(conf.def.data, conf.def.columns, conf.def.format, $elem) }
      redraw()
      
      $(window).on("resize", redraw)
      $scope.$watch(function () { return [$scope.time.T0, $scope.time.T1, $scope.time.ROUND, conf.def.columns] }, redraw, true)
    }
  })
  
  /* Holds the list of all test results and function to reload them. */
  .service('testData', function ($http, $rootScope) {
    var T = this
    
    T.tests = null
    
    T.init = function () {
      T.tests = []
      $rootScope.tests = T.tests
      
      _.each($rootScope.models, function (m) {
        m.mean = m.sigma = 0
        _.each($rootScope.models, function (n) {
          if (m.suffix > n.suffix) T.tests.push({a: m, b: n, result: "...", p: 0, diff: 0, interval: 0})
        })
      })
    }
    
    T.load = function () {
      return $http.get("/run-tests").then(function (response) {
        if (!T.tests) T.init()
        var data = response.data
        
        _.each($rootScope.models, function (m) { m.unavailable = true })
        
        _.each(T.tests, function (t) {
            var a = t.a.suffix, b = t.b.suffix, ab = a + "||" + b
            if (!data.summaries[a] || !data.summaries[b]) return
            _.extend(t.a, data.summaries[a], {unavailable: false})
            _.extend(t.b, data.summaries[b], {unavailable: false})
            _.extend(t, data.tests[ab])
        })
      })
    }
  })
  
  /* Initial test load, $watch for checkboxes to chenge chart columns,
     $watch for time interval to save and load new tests */
  .controller('body', function ($scope, $http, time, testData) {
    
    if ($scope.hasTests) {
      $scope.def.all_columns = $scope.def.columns
      _.each($scope.def.all_columns, function (c, i) { c.index = i })
      
      var resetColumns = function () {
        $scope.def.columns = _.filter($scope.def.all_columns, function (col) {
          var m = _.find($scope.models, function (m) { return col[0] == m.suffix })
          //TODO: search for a category too
          return !m || !m.unavailable && m.show
        })
      }
      
      $scope.$watch(function () { return _.map($scope.models, function(m) { return m.show }) }, function () {
        resetColumns()
      }, true)
      
      testData.load()
      
      $scope.$watch(function () { return [time.T0, time.T1] }, _.after(2, function () {
        time.save().then(function () {
          testData.load().then(function () {
            resetColumns()
          })
        })
      }), true)
      
    } else {
      $scope.$watch(function () { return [time.T0, time.T1] }, _.after(2, function () {
        time.save()
      }), true)
    }
  })

/* CHART DRAWING FUNCTIONS */

function redrawLineChart(source, columns, format, chart) {
  var column_titles = _.map(columns, function (c) { return {title: c[0], index: c.index} })
  var data = makeData(source, columns)
  chart.html("")
  drawLineChart(data, column_titles, format, chart)
}

function redrawBoxPlot(source, columns, format, chart) {
  var column_titles = _.map(columns, function (c) { return {title: c[0]} })
  var data = makeData(source, columns, "day")
  chart.html("")
  data = _.map(column_titles.slice(1), function (t, i) {
    d = _.filter(_.map(data, at(i+1)), function (v) { return v > 0; })
    if (d.length == 0) d = [0]
    d.sort(function (a, b) { return a-b })
    var q = function (n) {
      return d[Math.floor(n*d.length/100)]
    }
    return [t.title, q(9), q(25), q(50), q(75), q(91)]
  })
  drawBoxPlot(data, format.y, chart)
}

function randomColor() {
  var rnd = function (m, n) { return m+Math.floor(Math.random()*(n-m)) }
  var r = rnd(0, 256), g = rnd(10, 90), b = rnd(10, 90)
  return "rgb(" + r + "," + g + "," + b + ")"
}

function colorClass(n, color) {
  return ".color.c" + n + " { background-color: " + color + "; }\n" +
         "svg.chart path.c" + n + " { stroke: " + color + "; }\n" +
         "svg.chart .circle.c" + n + " .inner { fill: " + color + "; }\n"
}

function colorCSS(n0, n1) {
  var css = ""
  for (var i = n0; i < n1; i++) css += colorClass(i, randomColor())
  
  var head = $("head")
  var style = $("<style>")
  style.text(css)
  head.append(style)
}

function at(n) {
  return function(d) { return d[n] }
}

/* src is {param: [data], ..., index: [dates]},
   func is round_to.day or round_to.week
   returns same object, but with data grouped and index changed */
function group(src, func) {
  var res = {}
  _.each(src, function (arr, key) {
    res[key] = []
  })
  
  _.each(src.index, function (d, i) {
    ix = func(d)
    is_new = !_.last(res.index) || _.last(res.index).getTime() != ix.getTime()
    if (is_new) res.index.push(ix)
    _.each(src, function (arr, key) {
      if (key != "index") {
        if (is_new) res[key].push(src[key][i])
        else res[key][res[key].length-1] += src[key][i]
      }
    })
  })
  
  return res
}

//groups data columns according to ROUND, runs chart calculations
//  returns data prepared for drawLineChart
function makeData(source, columns, special_rounding) {
  source = group(source, time.round_to[special_rounding || time.ROUND])
  index = source["index"]
  
  //recursively evaluates a function on data columns
  //  func = ["*", arr, num] or ["/", arr, arr] or ["min", arr, num]
  function calculate(func) {
    if (_.isString(func))
      return source[func]
    else if (func[0] == "*" && _.isNumber(func[2]))
      return _.map(calculate(func[1]), function (v) { return v*func[2] })
    else if (func[0] == "*") {
      var d = calculate(func[2])
      return _.map(calculate(func[1]), function (n, i) { return n*d[i] })
    } else if (func[0] == "+") {
      var d = calculate(func[2])
      return _.map(calculate(func[1]), function (n, i) { return n+d[i] })
    } else if (func[0] == "/") {
      var d = calculate(func[2])
      return _.map(calculate(func[1]), function (n, i) { return (d[i] != 0 && n/d[i]) || 0 })
    } else if (func[0] == "min" && _.isNumber(func[2]))
      return _.map(calculate(func[1]), function (v) { return Math.min(v, func[2]) })
    else if (func[0] == "min") {
      var d = calculate(func[2])
      return _.map(calculate(func[1]), function (n, i) { return Math.min(n, d[i]) })
    }/* else if (func[0] == "normalize") {
      var d = calculate(func[1])
      var mean = _.foldl(d, function (x, m) { return x+m }, 0)/d.length
      var std = Math.sqrt(_.foldl(d, function (x, m) { return (x-mean)*(x-mean)+m }, 0)/d.length)
      return _.map(d, function (x) { return (x-mean)/std })
    }*/
  }
  
  var data_columns = _.map(columns, function (c, i) {
    return calculate(c[1])
  })
  
  var data_rows = _.map(index, function (d, i) {
    return _.map(data_columns, at(i))
  })
  
  return _.filter(data_rows, function (r) {
    return r[0] >= time.T0 && r[0] <= time.T1
  })
}

function drawLineChart(data, columns, format, $element) {

  //set up chart size and margins
  var margin = {top: 30, left: 60, bottom: 40, right: 20}
  var width = $element.parent().width() - margin.left - margin.right
  var height = MANY_CHARTS ? 250 : Math.min(Math.max($(window).height() - margin.top - margin.bottom - 290, 200), width)

  //find min/max of each column 
  var data = _.map(data, function (row) {
    var tail = row.slice(1)
    row.min = d3.min(tail)
    row.max = d3.max(tail)
    return row
  })

  //creates a format function
  //  fmt = [prefix, format, suffix]
  //  func = d3.time.format of d3.format
  //  dashDate = true if weeks should be shown as intervals
  function formatter(fmt, func, dashDate) {
      if (!fmt || !fmt[1]) return
      var mid = func(fmt[1])
      var base = function (n) { return fmt[0] + mid(n) + fmt[2]; }
      if (time.ROUND == "week" && dashDate)
        return function (n) {
          var m = new Date(n)
          m.setDate(n.getDate()+6)
          return base(n) + "-" + base(m)
        }
      else return base
  }

  //define scales and find ticks
  var x = d3.time.scale()
      .domain([data[0][0], data[data.length-1][0]])
      .range([margin.left, margin.left+width]);
  var ticksX = x.ticks(5)
  
  var tickMonths = _.map(ticksX, function (d) { return d.getMonth() })
  var sameMonth = _.any(tickMonths, function (m, i, ms) { return ms[i] == ms[i+1] })
  
  var fmtX = formatter(format.x[sameMonth ? "day": "month"], d3.time.format) || x.tickFormat(5)
  var fmtPopupX = formatter(format.x[time.ROUND], d3.time.format, true) || x.tickFormat(5)

  var y = d3.scale.linear()
      .domain([d3.min(data, at("min")), d3.max(data, at("max"))])
      .range([margin.top+height, margin.top]).nice();
  var ticksY = y.ticks(5)
  
  if (ticksY.length > 1) {
    d = ticksY[ticksY.length-1] - ticksY[ticksY.length-2]
    if (ticksY[ticksY.length-1]+d*0.1 < d3.max(data, at("max"))) {
      y.domain([d3.min(data, at("min")), d3.max(data, at("max")) + d])
      ticksY = y.ticks(6)
    }
  }
  
  var fmtY = formatter(format.y, d3.format) || y.tickFormat(5)
  var fmtPopupY = formatter(format.y, d3.format) || y.tickFormat(5)

  //build chart area
  var chart = d3.select($element.get(0))
      .attr("width", margin.left + width + margin.right)
      .attr("height", margin.top + height + margin.bottom);

  
  //build Y axis
  var line = chart.selectAll("g.line")
      .data(ticksY)
    .enter().append("g").attr("class", "line")
      .attr("transform", function (t) { return "translate(" + 0 + ", " + y(t) + ")" })
  
  line.append("line")
      .attr("x1", 0).attr("x2", margin.left+width+margin.right)
      .attr("y1", 0).attr("y2", 0)
  
  line.append("text")
      .attr("x", 0)
      .attr("y", -10)
      .text(fmtY);
  
  //build X axis
  var date = chart.selectAll("g.date")
      .data(ticksX)
    .enter().append("g").attr("class", "date")
      .attr("transform", function (t) { return "translate(" + x(t) + ", " + (margin.top+height) + ")"})
  
  date.append("line")
      .attr("x1", 0).attr("x2", 0)
      .attr("y1", 0).attr("y2", y(ticksY[ticksY.length-1])-(margin.top+height))
  
  date.append("text")
      .attr("x", 0)
      .attr("y", 20)
      .text(fmtX);
  
  chart.append("line")
      .attr("x1", 0).attr("x2", margin.right+width+margin.left)
      .attr("y1", margin.top+height).attr("y2", margin.top+height)
      .attr("class", "bottom")
  
  //build lines
  _.each(columns, function (col, i) {
    if (i == 0) return;
    
    var cl = "c"+(col.index || i)

    var zigzag = d3.svg.line()
        .x(function (d) { return x(d[0]) })
        .y(function (d) { return y(d[i]) })
    
    chart.append("path").attr("class", cl).attr("d", zigzag(data))
    
    var circle = chart.selectAll("g.circle."+cl)
        .data(data)
      .enter().append("g").attr("class", "circle "+cl)
        .attr("transform", function(d) { return "translate(" + x(d[0]) + "," + y(d[i]) + ")"; });

    circle.append("circle")
        .attr("class", "outer")
        .attr("r", 20)
    
    _.each(circle[0], function (out, j) {
        var d = data[j]
        
        //built tooltip
        var tip = $("<div>").addClass("tooltip box gradient small")
        $("<span>").addClass("date").appendTo(tip).text(fmtPopupX(d[0]))
        $("<br>").appendTo(tip)
        $("<span>").addClass("title caps").appendTo(tip).text(col.title + ": ")
        $("<span>").addClass("medium").appendTo(tip).text(fmtPopupY(d[i]))
        
        $(out).on("mouseenter", function (ev) {
            $element.after(tip)
            var off = $element.offset()
            tip.css({
                top: off.top + y(d[i]) -10,
                right: $(window).width() - (off.left + x(d[0]) + 20)})
        })
        
        $(out).on("mouseleave", function (ev) {
            tip.remove()
        })
    })
    
    circle.append("circle")
        .attr("class", "inner")
        .attr("r", 4)
  })
}


function drawBoxPlot(data, format, $element) {

  //set up chart size and margins
  var margin = {top: 30, left: 60, bottom: 40, right: 20}
  var width = $element.parent().width() - margin.left - margin.right
  var height = MANY_CHARTS ? 250 : Math.min(Math.max($(window).height() - margin.top - margin.bottom - 290, 200), width)

  //creates a format function
  //  fmt = [prefix, format, suffix]
  //  func = d3.format
  function formatter(fmt, func) {
      if (!fmt || !fmt[1]) return
      var mid = func(fmt[1])
      return function (n) { return fmt[0] + mid(n) + fmt[2]; }
  }

  //define scales and find ticks
  var boxW = (width - margin.left - margin.right)/data.length
  var x = function (name) {
    i = _.indexOf(_.pluck(data, 0), name)
    return margin.left + (i+0.5)*boxW
  }
  
  var y = d3.scale.linear()
      .domain([d3.min(data, function (col) { return col[1]}), d3.max(data, function (col) { return _.last(col) })])
      .range([margin.top+height, margin.top]).nice();
  var ticksY = y.ticks(5)
  
  if (ticksY.length > 1) {
    d = ticksY[ticksY.length-1] - ticksY[ticksY.length-2]
    if (ticksY[ticksY.length-1]+d*0.1 < d3.max(data, function (col) { return _.last(col) })) {
      y.domain([d3.min(data, function (col) { return col[1] }), d3.max(data, function (col) { return _.last(col) }) + d])
      ticksY = y.ticks(6)
    }
  }
  
  var fmtY = formatter(format, d3.format) || y.tickFormat(5)
  var fmtPopupY = fmtY
  
  //how much space will labels need? adjust chart height
  widest_label = 8*_.max(_.map(data, function (c) { return c[0].length; }))
  stack_height = Math.max(Math.ceil(widest_label/boxW), 1)
  margin.bottom += (stack_height-1)*20

  //build chart area
  var chart = d3.select($element.get(0))
      .attr("width", margin.left + width + margin.right)
      .attr("height", margin.top + height + margin.bottom)

  
  //build Y axis
  var line = chart.selectAll("g.line")
      .data(ticksY)
    .enter().append("g").attr("class", "line")
      .attr("transform", function (t) { return "translate(" + 0 + ", " + y(t) + ")" })
  
  line.append("line")
      .attr("x1", 0).attr("x2", margin.left+width+margin.right)
      .attr("y1", 0).attr("y2", 0)
  
  line.append("text")
      .attr("x", 0)
      .attr("y", -10)
      .text(fmtY);
  
  chart.append("line")
      .attr("x1", 0).attr("x2", margin.right+width+margin.left)
      .attr("y1", margin.top+height).attr("y2", margin.top+height)
      .attr("class", "bottom")
  
  //build boxes
  _.each(data, function (col, i) {
    var cl = "c"+(col.index || i)
    var key = col[0]
    var d = col.slice(1)

    var g = chart.append("g").attr("class", "candle " + cl)
        .attr("transform", "translate(" + x(key) + "," + 0 + ")");
    
    g.append("line")
        .attr("x1", 0).attr("x2", 0).attr("y1", y(d[0])).attr("y2", y(d[1]))
    
    g.append("line")
        .attr("x1", 0).attr("x2", 0).attr("y1", y(d[3])).attr("y2", y(d[4]))
    
    g.append("rect")
        .attr("x", -boxW/4).attr("y", y(d[3])).attr("width", boxW/2).attr("height", Math.abs(y(d[3])-y(d[1])))
    
    g.append("line")
        .attr("x1", -boxW/4).attr("x2", boxW/4).attr("y1", y(d[2])).attr("y2", y(d[2]))
    
    g.append("text").attr("x", 0).attr("y", margin.top+height+20*(i % stack_height + 1)).text(key)
        .attr("style", "text-anchor: middle")
    //g.append("text").attr("x", 0).attr("y", margin.top + height + 10)
    //    .attr("transform", "rotate(20, " + 0 + "," + (margin.top + height + 10) + ")").text(key)
  })
}
