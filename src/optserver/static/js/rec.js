angular.module('nn', [])
  .config(function($interpolateProvider) {
    $interpolateProvider.startSymbol('[[');
    $interpolateProvider.endSymbol('}}');
  })
  .controller('body', function ($scope, $http) {
    var items = $scope.items = _.sortBy(ITEMS, function (i) { return i.name })
    var notFound = {title: '[NOT FOUND]'}
    var getItem = function (id) {
      return _.find(items, function (j) { return j.id == id }) || notFound
    }
    
    _.each(items, function (i) {
      i.children = _.map(MAPPINGS[i.id], getItem)
    })
    
    var mkPages = function (ls) {
      $scope.pages = []
      var cnt = 50
      for(var i = 0; i < ls.length; i += cnt)
        $scope.pages.push(ls.slice(i, i+cnt))
      $scope.page = $scope.pages[0]
    }
    
    mkPages(items)
    
    $scope.setPage = function (n) {
      $scope.page = $scope.pages[n] || _.last($scope.pages)
    }
    
    $scope.searchPages = function (q) {
      if (!q) mkPages(items)
      else $http.get("/recs/search/"+q).then(function (res) {
        mkPages(_.map(res.data, getItem))
      }, function () {
        alert("ajax error")
      })
    }
    
    
    $scope.selected = null
    
    $scope.select = function (i) {
      $scope.selected = i
    }
    
    $scope.add = function (parent, child) {
      if (!_.contains(parent.children, child))
        $http.get("/add-mapping/"+parent.id+"/"+child.id).then(function () {
          parent.children.push(child)
        }, function () {
          alert("ajax error")
        })
    }
    
    $scope.rm = function (parent, child) {
      if (_.contains(parent.children, child))
        $http.get("/rm-mapping/"+parent.id+"/"+child.id).then(function () {
          parent.children.splice(_.indexOf(parent.children, child), 1)
        }, function () {
          alert("ajax error")
        })
    }
    
    $scope.has = function (parent, child) {
      return _.contains(parent.children, child)
    }

    $scope.getCurrentModel = function() {
      busyMouse();
      $http.get("/recs/download_model").then(function () {
        location.reload();
        freeMouse();
      }, function () {
        alert("ajax error")
        freeMouse();
      })
    }

    $scope.post_model = function(uniqueId) {
      busyMouse();
      $http.get("/recs/json/"+uniqueId).then(function () {
        alert("Model posted");
        freeMouse();
      }, function () {
        alert("ajax error");
        freeMouse();
      })
    }

    var busyMouse = function() {
      var body = angular.element( document.body );
      body.addClass('wait')
    }

    var freeMouse = function() {
      var body = angular.element( document.body );
      body.removeClass('wait')
    }
  })
