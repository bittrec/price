angular.module('nn', [])
  .config(function($interpolateProvider) {
    $interpolateProvider.startSymbol('[[');
    $interpolateProvider.endSymbol('}}');
  })
  .controller('body', function ($scope, $http) {
    var items = $scope.items = _.sortBy(ITEMS, function (i) { return i.name })
    var filter = $scope.filter = _.sortBy(FILTER, function (i) { return i.name })
    var notFound = {title: '[NOT FOUND]'}
    var getItem = function (id) {
      return _.find(items, function (j) { return j.id == id }) || notFound
    }
    
    var mkItemPages = function (ls) {
      $scope.item_pages = []
      var cnt = 50
      for(var i = 0; i < ls.length; i += cnt)
        $scope.item_pages.push(ls.slice(i, i+cnt))
      $scope.item_page = $scope.item_pages[0]
    }
    
    mkItemPages(items)
    
    $scope.setItemPage = function (n) {
      $scope.item_page = $scope.item_pages[n] || _.last($scope.item_pages)
    }
    
    $scope.searchItemPages = function (q) {
      if (!q) mkItemPages(items)
      else $http.get("/recs/search/"+q).then(function (res) {
        mkItemPages(_.map(res.data, getItem))
      }, function () {
        alert("ajax error")
      })
    }

    var mkFilterPages = function (ls) {
      $scope.filter_pages = []
      var cnt = 50
      for(var i = 0; i < ls.length; i += cnt)
        $scope.filter_pages.push(ls.slice(i, i+cnt))
      $scope.filter_page = $scope.filter_pages[0]
    }

    mkFilterPages(filter)

    $scope.setFilterPage = function (n) {
      $scope.filter_page = $scope.filter_pages[n] || _.last($scope.filter_pages)
    }

    $scope.searchFilterPages = function (q) {
      if (!q) mkFilterPages(filter)
    }

    $scope.filter = function (item) {
      if (!_.contains(filter, item))
          $http.get("/add-filter-rec/"+item.id).then(function () {
            item.filter = 1;
            filter.push(item);
            mkFilterPages(_.sortBy(filter, function (i) { return i.name }));
          }, function () {
            alert("ajax error")
          })
    }

    $scope.rm = function (item) {
      if (_.contains(filter, item))
          $http.get("/rm-filter-rec/"+item.id).then(function () {
            item.filter = 0;
            filter = _.without(filter, item);
            mkFilterPages(_.sortBy(filter, function (i) { return i.name }));
          }, function () {
            alert("ajax error")
          })
    }

    $scope.openInsertFilters = function() {
      window.open("/recs/filter_list", "Insert filter list", "width=400, height=400");
    }

    $scope.getCurrentModel = function() {
      busyMouse();
      $http.get("/recs/download_model").then(function () {
        location.reload();
        freeMouse();
      }, function () {
        alert("ajax error")
        freeMouse();
      })
    }

    $scope.post_model = function(uniqueId) {
      busyMouse();
      $http.get("/recs/filter_json/"+uniqueId).then(function () {
        alert("Model posted");
        freeMouse();
      }, function () {
        alert("ajax error");
        freeMouse();
      })
    }

    $scope.filter_recs = function(query){
      return function(param) {
        return _.indexOf(param.name, query) != -1
      }
    }

    var busyMouse = function() {
      var body = angular.element( document.body );
      body.addClass('wait')
    }

    var freeMouse = function() {
      var body = angular.element( document.body );
      body.removeClass('wait')
    }

  })
