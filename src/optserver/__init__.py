from flask import Flask, request
from reverse import ReverseProxied

configEnvVar = 'PRODUCTION_OPTSERVER_SETTINGS'
app = Flask(__name__)
app.wsgi_app = ReverseProxied(app.wsgi_app)
app.config.from_object('config')
try:
    app.config.from_envvar(configEnvVar)
    app.logger.info("Using config from " + str(configEnvVar))
except RuntimeError:
    app.logger.warn(str(configEnvVar) + " environment variable is not found, using default config.")

#SQLAlhemy
from flask.ext.sqlalchemy import SQLAlchemy
db = SQLAlchemy(app)

from flask_restful import Api
api = Api(app)

#Login Manager for dashboard
from flask.ext.login import LoginManager
login_manager = LoginManager()
login_manager.init_app(app)
login_manager.login_view = "/login"

#AUTHENTICATION FOR REST API and model views
from flask_httpauth import HTTPBasicAuth
#we use basic authentication for now
auth = HTTPBasicAuth()
#static user list from the config
users = app.config['USERS']

from flask_restless import ProcessingException

@auth.verify_password
def verify_password(username, password):
    if username in users and users.get(username) == password:
        return True
    else:
        return False

#restless
from flask_restless import APIManager
restmanager = APIManager(flask_sqlalchemy_db=db)


#HEALTH CHECK#
from healthcheck import HealthCheck, EnvironmentDump
health = HealthCheck(app, "/healthcheck")
envdump = EnvironmentDump(app, "/environment")

#shopify
import shopify

API_KEY = '911139577f2f896582786406abf3dbc8'
SHARED_SECRET = '31285f58e903d4834b601d77c05c7bae'
API_SCOPE = ['read_products', 'write_products', 'read_orders', 'read_customers', 'read_themes', 'write_themes', 'read_script_tags', 'write_script_tags']
APP_URL = 'https://kaina.bitrec.com/'

#read_content, write_content: Access to Article, Blog, Comment, Page, and Redirect.
#read_themes, write_themes: Access to Asset and Theme.
#read_products, write_products: Access to Product, Product Variant, Product Image, Collect, Custom Collection, and Smart Collection.
#read_customers, write_customers: Access to Customer and Customer Group.
#read_orders, write_orders: Access to Order, Transaction and Fulfillment.
#read_script_tags, write_script_tags: Access to Script Tag.
#read_fulfillments, write_fulfillments: Access to Fulfillment Service.
#read_shipping, write_shipping: Access to Carrier Service.

shopify.Session.setup(api_key=API_KEY, secret=SHARED_SECRET)

def open_shop(shop):
    """Creates a session for shopify API."""
    session = shopify.Session(shop.url, shop.shopify_token)
    shopify.ShopifyResource.activate_session(session)

def close_shop():
    """Closes any open shopify API sessions."""
    shopify.ShopifyResource.clear_session()

from optserver import restapi, models, views, dashboard


