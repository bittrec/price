# Statement for enabling the development environment
import os
DEBUG = True
#SHOW_ERRORS = True

basedir = os.path.abspath(os.path.dirname(__file__))

SQLALCHEMY_DATABASE_URI = 'sqlite:///' + os.path.join(basedir, 'app.db')
SQLALCHEMY_MIGRATE_REPO = os.path.join(basedir, 'db_repository')

# Application threads. A common general assumption is
# using 2 per available processor cores - to handle
# incoming requests using one and performing background
# operations using the other.
THREADS_PER_PAGE = 2

# Enable protection agains *Cross-site Request Forgery (CSRF)*
CSRF_ENABLED     = True
WTF_CSRF_ENABLED = True

# Use a secure, unique and absolutely secret key for
# signing the data.
CSRF_SESSION_KEY = "secrefdfsssdvcvzcx223423t"

# Secret key for signing cookies
SECRET_KEY = "secretfsfsadfaser32424rfdvgre"

#logging
LOGGING_FILE = "optserver.log"

#logstash events
LOGSTASH_LOGGING_FILE = "optserver_events.log"
LOGSTASH_MAX_SIZE = 1024*1024*10
LOGSTASH_FILE_COUNT = 9

SHOPS = [] #[1, 2]
#SHOP_PREDICATE = lambda s: s.id == 4

#usernames for API and admin views
USERS = {
    "bitrec": "bitbit",
    "pigu": "FA2fasl"
}


