#!flask/bin/python
import logging
from logging.handlers import RotatingFileHandler

from optserver import app, restmanager
from optserver.restapi import refresh_data

#IMPORTANT: we need to initialize restmanager here, because of the unittesting
#On initialization it takes app.config and modifies the database.
#If we do it ini __init__.py, it is not possible to make the tests work
restmanager.init_app(app)

#standard logger. We also have a logstash logger for all events in core
handler = RotatingFileHandler(app.config['LOGGING_FILE'], maxBytes=app.config['LOGSTASH_MAX_SIZE'], backupCount=1)
handler.setLevel(logging.DEBUG)
handler.setFormatter(logging.Formatter('%(asctime)s - %(name)s - %(levelname)s - %(message)s'))
app.logger.addHandler(handler)

app.logger.info('Starting the optserver')

from optserver import db
app.logger.info('"DB: ' + str(db))
app.logger.info('Starting cashing objects to memory')

refresh_data()
app.logger.info('Done cashing')

if __name__ == "__main__":
    app.run(host='0.0.0.0',threaded=True)
