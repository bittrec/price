'''
http://blog.miguelgrinberg.com/post/the-flask-mega-tutorial-part-iv-database
'''
from migrate.versioning import api
from config import SQLALCHEMY_DATABASE_URI
from config import SQLALCHEMY_MIGRATE_REPO
import os.path

from optserver import models,views,db,app, restmanager

app.config.from_object('config')
try:
    app.config.from_envvar('PRODUCTION_OPTSERVER_SETTINGS')
except RuntimeError:
    print "PRODUCTION_OPTSERVER_SETTINGS environment variable is not found, ignoring!"


print str(app.name)
print str(db)

db.create_all()
if not os.path.exists(SQLALCHEMY_MIGRATE_REPO):
    api.create(SQLALCHEMY_MIGRATE_REPO, 'database repository')
    api.version_control(SQLALCHEMY_DATABASE_URI, SQLALCHEMY_MIGRATE_REPO)
else:
    api.version_control(SQLALCHEMY_DATABASE_URI, SQLALCHEMY_MIGRATE_REPO, api.version(SQLALCHEMY_MIGRATE_REPO))

db.session.commit()
