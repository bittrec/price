# README #

Price optimization app.

# Installation #
Within python virtual environment execute pip install -r requirements.txt

# Deployment #
1. virtual env is located at ~/.venvs/optprice/
1. uwsgi config cat /etc/uwsgi/apps-enabled/optserver.ini
1. it points to head ~/optserver.conf
1. /etc/logstash/conf.d for transport to s3 

# Running #
1. Within directory runserver.py

# Devel #
## testing ##
nosetests --with-coverage --cover-erase --cover-package=optserver --cover-html
## curl ##
1. curl -X GET http://localhost:5000/healthcheck
